import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { Image } from 'tns-core-modules/ui/image';
import { Page } from 'tns-core-modules/ui/page';
import {AnimationCurve} from "tns-core-modules/ui/enums";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit {

    @ViewChild('logoImage') logoImage: ElementRef;
    @ViewChild('homePage') homePage: ElementRef;

    constructor(private _router: Router, private _page: Page) {}

    ngOnInit(): void {
        this._page.on('navigatedTo', () => {
            const homePage = <Page>this.homePage.nativeElement;
            const logoImage = <Image>this.logoImage.nativeElement;
            let pageSize =  homePage.getActualSize().width;

            logoImage.animate({
                opacity: 1,
                translate: { x: 0, y: pageSize / 2},
                duration: 2000,
                curve: AnimationCurve.spring
            });
          });
        
    }

    public goToOrderingNumbersPage() {
        this._router.navigate(['/ordering-numbers']);
    }
}
