import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Label } from 'tns-core-modules/ui/label';
import { AnimationCurve } from "tns-core-modules/ui/enums";
import { Page } from 'tns-core-modules/ui/page/page';
import { Number } from './number/number';
import { without } from "typescript-array-utils";
import { ValueList, DropDown } from "nativescript-drop-down";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { timer } from 'rxjs';

@Component({
  selector: 'ns-ordering-numbers',
  templateUrl: './ordering-numbers.component.html',
  styleUrls: ['./ordering-numbers.component.css'],
  moduleId: module.id,
})
export class OrderingNumbersComponent implements OnInit {

  //Game configuration
  public quantity = 2;
  public min = -99;
  public max = 99;
  public timerSeconds = 3;

  public selectedNumber;
  public points = 0;
  public counter = 0;
  public numbers: Number[] = [];
  public numbersToVerify: Number[] = [];
  public isCorrect = false;
  public message = '';
  public itemSource;
  public dd;
  public counterTime;
  public gameTimeout;

  @ViewChild('messageLabel') messageLabel: ElementRef;
  @ViewChild('page') page: ElementRef;
  @ViewChild('dd') dropDown: ElementRef;

  constructor() { }

  ngOnInit() {
    this.generateAleathoryNumbers(this.quantity, this.min, this.max);
    this.dd = <DropDown>this.dropDown.nativeElement;

    this.itemSource = new ValueList<string>([
      { value: "2", display: "Level 1" },
      { value: "3", display: "Level 2" },
      { value: "4", display: "Level 3" }
    ]);
    this.dd.items = this.itemSource;
    this.dd.selectedIndex = this.itemSource.getIndex("2");

  }

  public onchange(args: SelectedIndexChangedEventData) {
    this.timerSeconds = +(this.itemSource.getValue(args.newIndex)) + 1;
    this.quantity = this.itemSource.getValue(args.newIndex);
    this.generateAleathoryNumbers(this.quantity, this.min, this.max);
  }

  public selectNumber(selectedNumber) {
    let compareCounter = 0;
    this.numbersToVerify = without(this.numbersToVerify, this.numbersToVerify.findIndex(number => number.$number == selectedNumber));
    const indexOfSelectedNumber = this.numbers.findIndex(number => number.$number == selectedNumber);
    if (!this.numbers[indexOfSelectedNumber].$isCorrect) {
      for (const number of this.numbersToVerify) {
        if (selectedNumber <= number.$number) {
          compareCounter++;
          this.numbers[indexOfSelectedNumber].$isCorrect = true;
        } else {
          this.numbers[indexOfSelectedNumber].$isCorrect = false;
          this.animateError();
          this.points = 0;
          return;
        }
      }

      if (compareCounter == this.numbersToVerify.length) {
        this.counter++;
        this.validatePoint();
      }
    }
  }

  private async generateAleathoryNumbers(quantity, min, max) {
    this.counter = 0;
    this.numbers = [];
    this.numbersToVerify = [];
    this.message = '';
    while (this.numbers.length < quantity) {
      const plusOrMinus = Math.random() < 0.5 ? -1 : 1;
      const number = Math.floor((Math.random() * max) + min) * plusOrMinus;
      if (!(this.numbers.findIndex(arrayNumber => arrayNumber.$number == number) >= 0)) {
        this.numbers.push(new Number(number));
        this.numbersToVerify.push(new Number(number));
      }
    }

    await clearTimeout(this.gameTimeout);
    await this.timer(this.timerSeconds);
  }

  private validatePoint() {
    if (this.counter == this.quantity) {
      this.points++;
      this.generateAleathoryNumbers(this.quantity, this.min, this.max);
    } else {
      if (this.counterTime === 0) {
        this.points = 0;
      }
    }
  }

  private async animateError() {
    const messageLabel = <Label>this.messageLabel.nativeElement;
    const page = <Page>this.page.nativeElement;
    let pageSize = await page.getActualSize().width;

    messageLabel.animate({
      opacity: 1,
      translate: { x: pageSize / 3, y: 0 },
      duration: 1000,
      curve: AnimationCurve.spring
    }).then(async () => {
      messageLabel.translateX = 0;
      messageLabel.opacity = 0;
      await this.generateAleathoryNumbers(this.quantity, this.min, this.max);
    });
  }

  private timer(seconds: number) {
    if (seconds >= 0) {
      this.gameTimeout = setTimeout(async () => {
        this.counterTime = seconds;
        seconds--;
        this.timer(seconds);
      }, 1000);
    } else {
      this.validatePoint();
      this.generateAleathoryNumbers(this.quantity, this.min, this.max);
    }
  }
}
