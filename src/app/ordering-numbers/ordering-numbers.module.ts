import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import {NativeScriptFormsModule} from "nativescript-angular/forms";
import { OrderingNumbersRoutingModule } from "./ordering-numbers-routing.module";
import { OrderingNumbersComponent } from "./ordering-numbers.component";
import { NumberComponent } from './number/number.component';
import { DropDownModule } from "nativescript-drop-down/angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        OrderingNumbersRoutingModule,
        NativeScriptFormsModule,
        DropDownModule
    ],
    declarations: [
        OrderingNumbersComponent,
        NumberComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class OrderingNumbersModule { }
