export class Number {
    private number: number;
    private isCorrect: boolean;

    constructor(number: number) {
        this.number = number;
    }

    /**
     * Getter $number
     * @return {number}
     */
	public get $number(): number {
		return this.number;
	}

    /**
     * Getter $isCorrect
     * @return {boolean}
     */
	public get $isCorrect(): boolean {
		return this.isCorrect;
	}

    /**
     * Setter $number
     * @param {number} value
     */
	public set $number(value: number) {
		this.number = value;
	}

    /**
     * Setter $isCorrect
     * @param {boolean} value
     */
	public set $isCorrect(value: boolean) {
		this.isCorrect = value;
	}
}