import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { Button } from 'tns-core-modules/ui/button';
import { Color } from 'tns-core-modules/color/color';

@Component({
  selector: 'ns-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css'],
  moduleId: module.id,
})
export class NumberComponent implements OnInit, OnChanges {
  
  @Input() number;
  @Input() isCorrect;
  @Input() col;
  @Output() selectNumber = new EventEmitter<string>();
  @ViewChild('button') button: ElementRef;

  constructor() { }

  ngOnInit() { 
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
      if(changes.isCorrect) {
      this.selectColor();
    }
  }

  public async onSelectNumber() {
    this.selectNumber.emit(this.number);
  }

  public async selectColor() {
    const button = <Button> this.button.nativeElement;
    let color;
    if (this.isCorrect) {
      color = new Color('#4bd881');
    } else if (this.isCorrect == false) {
      color = new Color('tomato');
    } else {
      color = new Color('#5ebeff');
    }

    await button.animate({
      backgroundColor: color,
			duration: 300
    });
  }

}
