import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page';
import { Router } from '@angular/router';


@Component({
  selector: 'ns-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css'],
  moduleId: module.id,
})
export class IntroComponent implements OnInit {

  @ViewChild('introPage') introPage: ElementRef;

  constructor(private _page: Page,private _router: Router) { }

  ngOnInit() {

    this._page.on('navigatedTo', () => {
      const introPage = <Page>this.introPage.nativeElement;  
      introPage.animate({
          opacity: 1,
          duration: 3000
      })
      .then(() => {
        introPage.animate({
          opacity: 0,
          duration: 3000
      })
      .then(()=> {
        this._router.navigate(['/home']);
      });
      });
    });  
  }

}
