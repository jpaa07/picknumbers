import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/intro", pathMatch: "full" },
    { path: "intro", loadChildren: "~/app/intro/intro.module#IntroModule" },
    { path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
    { path: "ordering-numbers", loadChildren: "~/app/ordering-numbers/ordering-numbers.module#OrderingNumbersModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
